https://www.exoscale.com/syslog/setup-private-docker-registry/

For an image stored in a registry to be run requires it to be first pulled to a Docker instance.

There’s one constraint though, the image’s name needs to be prefixed with the registry’s URL (whether domain-based or IP), port included e.g. 159.100.243.157:5000/hello-world.
This is the responsibility of the docker tag command:
```sh
$ sudo docker tag {your image name} {your registry ip}:5000/{your image name}
```
ex :
```sh
$ sudo docker tag {your image name} {your registry ip}:5000/{your image name}
```

The new label should now appear with the command:
```sh
$ sudo docker images
```

